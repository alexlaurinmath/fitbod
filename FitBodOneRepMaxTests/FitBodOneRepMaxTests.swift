//
//  FitBodOneRepMaxTests.swift
//  FitBodOneRepMaxTests
//
//  Created by Alexandre Laurin on 12/10/17.
//  Copyright © 2017 Snacktime. All rights reserved.
//

import XCTest
@testable import FitBodOneRepMax

class FitBodOneRepMaxTests: XCTestCase {
    
    var dataLoaderUnderTest: WorkoutDataLoader!
    var exerciseTableViewControllerUnderTest: ExerciseTableViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        let bundle = Bundle.main
//        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        
        dataLoaderUnderTest = WorkoutDataLoader()
        exerciseTableViewControllerUnderTest = ExerciseTableViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        dataLoaderUnderTest = nil
        exerciseTableViewControllerUnderTest = nil
    }
    
    func testWorkoutDataIsLoaded() {
        let numberOfAddedWorkouts:Int = 582
        let workouts = dataLoaderUnderTest.loadData(withFileName: "workoutData")
        XCTAssertEqual(workouts.count,numberOfAddedWorkouts,"The number of workouts from data is wrong.")
        
        for workout in workouts {
            XCTAssertNotNil(workout.name,"Name missing for a workout with date \(workout.date ?? Date())")
            XCTAssertNotNil(workout.date,"Date missing for a workout with name \(workout.name ?? "no name")")
            XCTAssertNotNil(workout.numberOfReps,"Missing number of reps for a workout with name \(workout.name ?? "no name")")
            XCTAssertNotNil(workout.numberOfSets,"Missing number of sets for a workout with name \(workout.name ?? "no name")")
            XCTAssertNotNil(workout.weights,"Missing weights for a workout with name \(workout.name ?? "no name")")
        }
    }
    
    func testExercisesAreCreated() {
        let numberOfAddedExercises:Int = 3
        let exerciseNames = ["Back Squat","Barbell Bench Press","Back Squat"]
        let workouts = dataLoaderUnderTest.loadData(withFileName: "workoutData")
        let exercises = dataLoaderUnderTest.parseWorkouts(workouts)
        XCTAssertEqual(exercises.count,numberOfAddedExercises,"The exercises are not getting created properly by the workouts.")
        for exerciseIndex in 0..<exercises.count {
            XCTAssertNotNil(exerciseNames.index(of: exerciseNames[exerciseIndex]),"Exercise names are getting misplaced.")
        }
    }
    
    func testMergeDailyValues() {
        let numberOfAddedExercises:Int = 3
        let exerciseNumberOfDays = ["Back Squat":30, "Barbell Bench Press":30, "Deadlift":28]
        let workouts = dataLoaderUnderTest.loadData(withFileName: "workoutData")
        let exercises = dataLoaderUnderTest.parseWorkouts(workouts)
        for exercise in exercises {
            exercise.mergeDailyValues()
        }
        XCTAssertEqual(exercises.count,numberOfAddedExercises,"Number of exercises should not be altered from merging.")
        for exercise in exercises {
            XCTAssertEqual(exercise.workouts.count, exerciseNumberOfDays[exercise.name!], "number of workout days for \(exercise.name ?? "unkown name") is wrong.")
        }
    }
    
    func testComputeOneRepMax() {
        let exerciseValuesDict = ["Back Squat":217, "Barbell Bench Press":179, "Deadlift":254]
        let workouts = dataLoaderUnderTest.loadData(withFileName: "workoutData")
        let exercises = dataLoaderUnderTest.parseWorkouts(workouts)
        for exercise in exercises {
            exercise.mergeDailyValues()
            exercise.computeOneRepMax()
        }
        
        for exercise in exercises {
            XCTAssertEqual(exercise.value, exerciseValuesDict[exercise.name!], "one rep max not computing correctly for \(exercise.name ?? "unknown name")")
        }
    }
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
