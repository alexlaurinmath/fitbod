//
//  Erercise.swift
//  FitBodOneRepMax
//
//  Created by Alexandre Laurin on 12/9/17.
//  Copyright © 2017 Snacktime. All rights reserved.
//

import Foundation

class Exercise {
    var name: String?
    var detail: String = "One Rep Max ● lbs"
    var workouts:[Workout] = []
    var value: Int?
    
    func mergeDailyValues() {
        // all workouts that were done on the same day should actually be the same workout
        sortWorkouts()
        if workouts.count > 0 {
            var tempWorkouts:[Workout] = [workouts[0]]
            for workoutIndex in 1..<workouts.count {
                let _workout = workouts[workoutIndex]
                // check for same date, only check last because they are sorted
                if _workout.date == tempWorkouts.last!.date {
                    if let minCount = [_workout.numberOfReps.count,_workout.weights.count].min() {
                        // deal with number of sets
                        if let _numberOfSets = _workout.numberOfSets {
                            for _ in 0..<_numberOfSets {
                            //update number of sets
                                if let _ = tempWorkouts.last!.numberOfSets {
                                    tempWorkouts.last!.numberOfSets! += minCount
                                } else {
                                    tempWorkouts.last!.numberOfSets = minCount
                                }
                                // update number of reps and weights
                                for count in 0..<minCount {
                                    tempWorkouts.last!.numberOfReps.append(_workout.numberOfReps[count])
                                    tempWorkouts.last!.weights.append(_workout.weights[count])
                                }
                            }
                        }
                    }
                } else {
                    // create workout, deal with number of sets
                    if let _numberOfSets = _workout.numberOfSets {
                        for _ in 0..<_numberOfSets {
                            let singleSetWorkout = _workout
                            singleSetWorkout.numberOfSets = 1
                            tempWorkouts.append(singleSetWorkout)
                        }
                    }
                }
            }
            workouts = tempWorkouts
        }
    }
    
    func computeOneRepMax() {
        var allValues:[Int] = []
        // obtain
        for workout in self.workouts{
            workout.computeOneRepMax()
            if workout.oneRepMax != nil {
                allValues.append(workout.oneRepMax!)
            }
        }
        if allValues.count > 0 {
            self.value = allValues.reduce(0,+)/allValues.count
        }
    }
    
    // sort all workouts according to date
    func sortWorkouts(){
        var dates:[Double] = []
        for workout in workouts {
            if let _date = workout.date {
                dates.append(_date.timeIntervalSince1970)
            } else {
                print("No date for workout with name \(workout.name ?? "unkown name")")
            }
        }
        // use zip to combine the two arrays and sort that based on the first
        let combined = zip(dates, workouts).sorted {$0.0 < $1.0}
        workouts = combined.map {$0.1}
    }
}
