//
//  ExerciseDataLoader.swift
//  FitBodOneRepMax
//
//  Created by Alexandre Laurin on 12/9/17.
//  Copyright © 2017 Snacktime. All rights reserved.
//

import Foundation


class WorkoutDataLoader {
    
    func loadData(withFileName:String) -> [Workout] {
        
        var workouts:[Workout] = []
        
        // load text file as data
        let bundle = Bundle.main
        let path = bundle.path(forResource: withFileName, ofType: "txt")
        var data: String?
        
        // turn data into a String
        do {
            data = try String(contentsOfFile:path!, encoding:String.Encoding.utf8)
        } catch _ as NSError {
            data = "Oct 11 2017,Back Squat,1,10,45"
        }
        
        // split in rows
        let rows = data!.components(separatedBy: "\r\n")
        for row in rows {
            let workout = Workout()
            let elements = row.components(separatedBy: ",")
            
            // go through the row and set workout variables from input
            for elemIndex in 0..<elements.count {
                switch elemIndex {
                    case 0:
                        workout.date = workoutDate(fromString: elements[elemIndex])
                    case 1:
                        workout.name = elements[elemIndex]
                    case 2:
                        workout.numberOfSets = Int(elements[elemIndex])
                    case 3:
                        if let numberOfReps = Int(elements[elemIndex]) {
                            workout.numberOfReps.append(numberOfReps)
                        } else {
                            print("problem with number of reps in data loader")
                        }
                    case 4:
                        if let weight = Int(elements[elemIndex]) {
                            workout.weights.append(weight)
                        } else {
                            print("problem with weight in data loader")
                        }
                    default:
                        print("no preset translation for element \(elements[elemIndex]) at index \(elemIndex)")
                }
            }
            if let _ = workout.name, let _ = workout.date, let _ = workout.numberOfSets {
                workouts.append(workout)
            }
        }
        return workouts
    }
    
    func parseWorkouts(_ workouts:[Workout]) -> [Exercise] {
        var exercises:[Exercise] = []
        for workout in workouts {
            if exercises.count > 0 {
                // check if that kind of exercise has already been logged
                var exerciseDidExist:Bool = false
                for exercise in exercises {
                    if exercise.name == workout.name {
                        exerciseDidExist = true
                        exercise.workouts.append(workout)
                    }
                }
                // if no such exercise exists, create one
                if !exerciseDidExist && workout.name != nil {
                    print("found new workout with name \(workout.name ?? "no name")")
                    let exercise = Exercise()
                    exercise.name = workout.name
                    exercise.workouts = []
                    exercise.workouts.append(workout)
                    exercises.append(exercise)
                }
            } else if workout.name != nil {
                // create the first exercise
                print("found first workout with name \(workout.name ?? "no name")")
                let exercise = Exercise()
                exercise.name = workout.name
                exercise.workouts = []
                exercise.workouts.append(workout)
                exercises.append(exercise)
            }
        }
        return exercises
    }
    
    private func workoutDate(fromString:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = workoutDateFormat
        let date = dateFormatter.date(from: fromString)
        return date
    }
}
