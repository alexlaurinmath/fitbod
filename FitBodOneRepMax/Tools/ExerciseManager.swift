//
//  ExerciseManager.swift
//  FitBodOneRepMax
//
//  Created by Alexandre Laurin on 12/11/17.
//  Copyright © 2017 Snacktime. All rights reserved.
//

import Foundation

let workoutDataFileName: String = "workoutData"

// Notification when exercises are modified
let exerciseManagerExercisesChangedNotification = "exerciseManagerExercisesChanged"

// Exercise Manager singleton
private let _shared = ExerciseManager()

final class ExerciseManager {
    
    class var shared: ExerciseManager {
        return _shared
    }
    
    fileprivate var _exercises: [Exercise] = []
    
    fileprivate let exerciseQueue =
        DispatchQueue(
            label: "exerciseQueue",
            attributes: .concurrent)
    
    fileprivate let workoutQueue =
        DispatchQueue(
            label: "workoutQueue",
            attributes: .concurrent)
    
    var exercises: [Exercise] {
        // handling readers, writers problem
        var exercisesCopy: [Exercise]!
        exerciseQueue.sync {
            exercisesCopy = self._exercises
        }
        return exercisesCopy
    }
    
    let dataLoader = WorkoutDataLoader()
    
    init() {
        loadAllWorkouts()
    }
    
    func updateExercises(with:[Exercise]) {
        
        exerciseQueue.async(flags: .barrier) {
            self._exercises = with
            DispatchQueue.main.async {
                self.postExercisesChangedNotification()
            }
        }
    }
    
    func loadAllWorkouts() {
        print("Loading Workouts.")
        self._exercises = []
        workoutQueue.async {
            // read workout data text file
            let workouts = self.dataLoader.loadData(withFileName: workoutDataFileName)
            // parse workout data to obtain exercises
            var tempExercises = self.dataLoader.parseWorkouts(workouts)
            self.updateExercises(with: tempExercises)
            tempExercises = self.mergeExercises(tempExercises)
            self.updateExercises(with: tempExercises)
            tempExercises = self.computeOneRepMaxForAll(tempExercises)
            self.updateExercises(with: tempExercises)
        }
    }
    
    private func mergeExercises(_ mergeExercises:[Exercise]) -> [Exercise] {
        print("merging daily values")
        // merge all workouts that were done the same day
        for exercise in mergeExercises {
            exercise.mergeDailyValues()
        }
        return mergeExercises
    }
    
    private func computeOneRepMaxForAll(_ oneRepMaxExercises:[Exercise]) -> [Exercise] {
        print("computing one rep max")
        for exercise in oneRepMaxExercises {
            exercise.computeOneRepMax()
        }
        
        return oneRepMaxExercises
    }
    
    fileprivate func postExercisesChangedNotification() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: exerciseManagerExercisesChangedNotification), object: nil)
    }
}

/*
 ______________$$$$$$$$$$____________________
 _____________$$__$_____$$$$$________________
 _____________$$_$$__$$____$$$$$$$$__________
 ____________$$_$$__$$$$$________$$$_________
 ___________$$_$$__$$__$$_$$$__$$__$$________
 ___________$$_$$__$__$$__$$$$$$$$__$$_______
 ____________$$$$$_$$_$$$_$$$$$$$$_$$$_______
 _____________$$$$$$$$$$$$$_$$___$_$$$$______
 ________________$$_$$$______$$$$$_$$$$______
 _________________$$$$_______$$$$$___$$$_____
 ___________________________$$_$$____$$$$____
 ___________________________$$_$$____$$$$$___
 __________________________$$$$$_____$$$$$$__
 _________________________$__$$_______$$$$$__
 ________________________$$$_$$________$$$$$_
 ________________________$$$___________$$$$$_
 _________________$$$$___$$____________$$$$$$
 __$$$$$$$$____$$$$$$$$$$_$____________$$$_$$
 _$$$$$$$$$$$$$$$______$$$$$$$___$$____$$_$$$
 $$________$$$$__________$_$$$___$$$_____$$$$
 $$______$$$_____________$$$$$$$$$$$$$$$$$_$$
 $$______$$_______________$$_$$$$$$$$$$$$$$$_
 $$_____$_$$$$$__________$$$_$$$$$$$$$$$$$$$_
 $$___$$$__$$$$$$$$$$$$$$$$$__$$$$$$$$$$$$$__
 $$_$$$$_____$$$$$$$$$$$$________$$$$$$__$___
 $$$$$$$$$$$$$$_________$$$$$______$$$$$$$___
 $$$$_$$$$$______________$$$$$$$$$$$$$$$$____
 $$__$$$$_____$$___________$$$$$$$$$$$$$_____
 $$_$$$$$$$$$$$$____________$$$$$$$$$$_______
 $$_$$$$$$$hg$$$____$$$$$$$$__$$$____________
 $$$$__$$$$$$$$$$$$$$$$$$$$$$$$______________
 $$_________$$$$$$$$$$$$$$$__________________
 */
