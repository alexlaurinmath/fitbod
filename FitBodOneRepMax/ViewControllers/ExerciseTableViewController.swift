//
//  ExerciseTableViewController.swift
//  FitBodOneRepMax
//
//  Created by Alexandre Laurin on 12/9/17.
//  Copyright © 2017 Snacktime. All rights reserved.
//

import UIKit

class ExerciseTableViewController: UITableViewController {

    var exercises:[Exercise] = []
    let dataLoader = WorkoutDataLoader()
    var selectedCell:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load cell nibs
        tableView.register(UINib(nibName: "ExerciseCellNib", bundle: nil), forCellReuseIdentifier: "exerciseCell")
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contentChangedNotification(_:)),
            name: NSNotification.Name(rawValue: exerciseManagerExercisesChangedNotification),
            object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ExerciseManager.shared.exercises.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell", for: indexPath) as! ExerciseTableViewCell
        // Configure the cell...
        let exercises = ExerciseManager.shared.exercises
        let exercise = exercises[indexPath.row]
        
        cell.exerciseView.nameLabel.text = exercise.name
        cell.exerciseView.detailLabel.text = exercise.detail
        cell.exerciseView.valueLabel.text = "\(exercise.value ?? 0)"
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCell = indexPath
        self.performSegue(withIdentifier: "exerciseDetailsSegue", sender: self.tableView(self.tableView, cellForRowAt: indexPath))
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "exerciseDetailsSegue"{
            if let detailsViewController = segue.destination as? DetailsViewController, let indexPath = selectedCell {
                let exercises = ExerciseManager.shared.exercises
                let exercise = exercises[indexPath.row]
                detailsViewController.exercise = exercise
            }
        }
    }

}

extension ExerciseTableViewController {
    @objc func contentChangedNotification(_ notification: Notification!) {
        tableView.reloadData()
    }
}
